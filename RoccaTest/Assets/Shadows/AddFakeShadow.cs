﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddFakeShadow : MonoBehaviour {

    [SerializeField]
    private GameObject shadow;

	// Use this for initialization
	void Start () {
        Vector3 extents = gameObject.GetComponent<BoxCollider>().bounds.extents;
        GameObject realShadow = Instantiate(shadow, gameObject.transform);
        realShadow.transform.localScale = new Vector3(1, 1, 1); //not ok for everything

        realShadow.transform.position = gameObject.transform.position - new Vector3(0, extents.y - 0.005f , 0);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
