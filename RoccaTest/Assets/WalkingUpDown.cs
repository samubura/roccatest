﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalkingUpDown : MonoBehaviour {

    public enum Axis
    {
        X, Z
    }

    public Axis axis;
    [SerializeField]
    private int  walkTime = 400;
    private int rotationTime;
    private int frames;
    private float velocity = 0.022f;

    // Use this for initialization
    void Start() {
        frames = 0;
        rotationTime = walkTime + 50;
        velocity = velocity * gameObject.transform.localScale.z;
    }

    // Update is called once per frame
    void Update() {
        if (frames < walkTime)
        {
            if(axis == Axis.X)
                gameObject.transform.position += gameObject.transform.right * velocity;
            if (axis == Axis.Z)
                gameObject.transform.position += gameObject.transform.forward * velocity;
        }
        else if(frames < rotationTime) {
            gameObject.transform.Rotate(Vector3.up, 180 * 1/50f);
        } else
        {
            frames = 0;
            return;
        }
        frames++;
	}
}
