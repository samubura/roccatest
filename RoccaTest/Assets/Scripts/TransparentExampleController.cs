//-----------------------------------------------------------------------
// <copyright file="AugmentedImageExampleController.cs" company="Google">
//
// Copyright 2018 Google Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// </copyright>
//-----------------------------------------------------------------------

    using System.Collections.Generic;
    using System.Runtime.InteropServices;
    using GoogleARCore;
    using UnityEngine;
    using UnityEngine.UI;

/// <summary>
/// Controller for example.
/// </summary>
public class TransparentExampleController : MonoBehaviour
{

    /// <summary>
    /// The first-person camera being used to render the passthrough camera image (i.e. AR background).
    /// </summary>
    public Camera FirstPersonCamera;
    /// <summary>
    /// A prefab for visualizing an AugmentedImage.
    /// </summary>
    public GameObject prefab;

    private GameObject realPrefab = null;

    /// <summary>
    /// The overlay containing the fit to scan user guide.
    /// </summary>
    public GameObject FitToScanOverlay;

    private AugmentedImage trackedImage;

    private Anchor anchor;

    private List<AugmentedImage> m_TempAugmentedImages = new List<AugmentedImage>();

    /// <summary>
    /// The Unity Update method.
    /// </summary>
    public void Update()
    {
        // Exit the app when the 'back' button is pressed.
        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }

        // Check that motion tracking is tracking.
        if (Session.Status != SessionStatus.Tracking)
        {
            return;
        }

        // Get updated augmented images for this frame.
        Session.GetTrackables<AugmentedImage>(m_TempAugmentedImages, TrackableQueryFilter.Updated);

        // Create visualizers and anchors for updated augmented images that are tracking and do not previously
        // have a visualizer. Remove visualizers for stopped images.
        foreach (var image in m_TempAugmentedImages)
        {
            if (image.TrackingState == TrackingState.Tracking && realPrefab == null)
            {
                trackedImage = image;

                // Raycast against the location of the player to search for the first vertical plane.
                TrackableHit hit;
                TrackableHitFlags raycastFilter = TrackableHitFlags.PlaneWithinPolygon;

                if (Frame.Raycast(Screen.width / 2, Screen.height / 2, raycastFilter, out hit))
                {
                    // Use hit pose and camera pose to check if hittest is from the
                    // back of the plane, if it is, no need to create the anchor.
                    if ((hit.Trackable is DetectedPlane) &&
                        Vector3.Dot(FirstPersonCamera.transform.position - hit.Pose.position,
                            hit.Pose.rotation * Vector3.up) < 0)
                    {
                        Debug.Log("Hit at back of the current DetectedPlane");
                    }
                    else if (((DetectedPlane)hit.Trackable).PlaneType == DetectedPlaneType.Vertical) //if i get a vertical plane
                    {
                        Pose prefabPose;
                        prefabPose = image.CenterPose;
                        anchor = hit.Trackable.CreateAnchor(prefabPose);

                        realPrefab = Instantiate(prefab, anchor.transform);
                        realPrefab.transform.Rotate(Vector3.right, 90f);
                        realPrefab.transform.Rotate(Vector3.up, 85f);
                        realPrefab.transform.Translate(-0.98f, -1.30f, -0.9f);
                    }
                }
                else if (image.TrackingState == TrackingState.Stopped && realPrefab != null)
                {
                    Destroy(realPrefab);
                    realPrefab = null;
                    trackedImage = null;
                }
            }

            if (trackedImage != null)
            {
                // Show the fit-to-scan overlay if there are no images that are Tracking.
                if (trackedImage.TrackingState == TrackingState.Tracking)
                {
                    FitToScanOverlay.SetActive(false);
                    return;
                }
            }

            FitToScanOverlay.SetActive(true);
        }
    }
}