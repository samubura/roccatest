﻿using GoogleARCore;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour {

    public Camera FirstPersonCamera;

    public GameObject prefab;

    public GameObject FitToScanOverlay;

    public GameObject planeGenerator;

    private bool m_IsQuitting = false;
    private AugmentedImage trackedImage = null;

    private GameObject realPrefab = null;

    private List<AugmentedImage> m_TempAugmentedImages = new List<AugmentedImage>();

    private static bool isInstantiated = false;
	
	void Update () {
        _UpdateApplicationLifecycle();

        Session.GetTrackables(m_TempAugmentedImages, TrackableQueryFilter.Updated);

        foreach (var image in m_TempAugmentedImages)
        {
            if (image.TrackingState == TrackingState.Tracking && realPrefab == null)
            {
                Vector3 imageCenterOnScreen = FirstPersonCamera.WorldToScreenPoint(image.CenterPose.position);
                
                //I've found for the first time the Augmented Image (marker), I'm going to find a plane.

                TrackableHit hit;
                
                if (Frame.Raycast(imageCenterOnScreen.x, imageCenterOnScreen.y, TrackableHitFlags.PlaneWithinPolygon, out hit))
                {
                    //plane found
                    // Use hit pose and camera pose to check if hittest is from the
                    // back of the plane, if it is, no need to create the anchor.

                    bool isHitFromTheBackOfThePlane = Vector3.Dot(FirstPersonCamera.transform.position - hit.Pose.position, hit.Pose.rotation * Vector3.up) < 0;

                    if ((hit.Trackable is DetectedPlane) && isHitFromTheBackOfThePlane)
                    {
                        Debug.Log("Hit at back of the current DetectedPlane");
                    }
                    else 
                    {
                        DetectedPlane plane = (DetectedPlane)hit.Trackable;

                        //Pose prefabPose = new Pose(image.CenterPose.position, plane.CenterPose.rotation); //???
                        
                        var anchor = plane.CreateAnchor(image.CenterPose);// prefabPose);

                        trackedImage = image;

                        realPrefab = Instantiate(prefab, anchor.transform);
                        
                        realPrefab.transform.Rotate(Vector3.right, 90f);
                        realPrefab.transform.Rotate(Vector3.up, 85f);
                        realPrefab.transform.Translate(-0.98f, -1.30f, -0.9f);

                        planeGenerator.SetActive(false);
                        isInstantiated = true;
                    }
                }
                else
                {
                    return;
                }
            }
            else if (image.TrackingState == TrackingState.Stopped && realPrefab != null)
            {
                //lost tracking
                Destroy(realPrefab);

                realPrefab = null;
                trackedImage = null;
            }
        }

        // Show the fit-to-scan overlay if there are no images that are Tracking.
        if (trackedImage != null)
        {
            if (trackedImage.TrackingState == TrackingState.Tracking)
            {
                FitToScanOverlay.SetActive(false);
                return;
            }
        }

        FitToScanOverlay.SetActive(true);
    }

    public static bool isRoccaInstantiated()
    {
        return isInstantiated;
    }

    private void _UpdateApplicationLifecycle()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }

        if (Session.Status != SessionStatus.Tracking)
        {
            const int lostTrackingSleepTimeout = 15;
            Screen.sleepTimeout = lostTrackingSleepTimeout;
        }
        else
        {
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
        }

        if (m_IsQuitting)
        {
            return;
        }

        if (Session.Status == SessionStatus.ErrorPermissionNotGranted)
        {
            _ShowAndroidToastMessage("Camera permission is needed to run this application.");
            m_IsQuitting = true;
            Invoke("_DoQuit", 0.5f);
        }
        else if (Session.Status.IsError())
        {
            _ShowAndroidToastMessage("ARCore encountered a problem connecting.  Please start the app again.");
            m_IsQuitting = true;
            Invoke("_DoQuit", 0.5f);
        }
    }

    private void _DoQuit()
    {
        Application.Quit();
    }

    private void _ShowAndroidToastMessage(string message)
    {
        AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject unityActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

        if (unityActivity != null)
        {
            AndroidJavaClass toastClass = new AndroidJavaClass("android.widget.Toast");
            unityActivity.Call("runOnUiThread", new AndroidJavaRunnable(() =>
            {
                AndroidJavaObject toastObject = toastClass.CallStatic<AndroidJavaObject>("makeText", unityActivity, message, 0);
                toastObject.Call("show");
            }));
        }
    }
}
